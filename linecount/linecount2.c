#include <stdio.h>
#include <stdbool.h>

bool is_vowel(int c) {
    switch (c) {
      case 'a':
      case 'e':
      case 'i':
      case 'o':
      case 'u':
      case 'A':
      case 'E':
      case 'I':
      case 'O':
      case 'U':
        return true;
      default:
        return false;
    }
}

int main(void) {
  int c = 0;
  int lines = 0;
  int vowels = 0;


  while ((c = getchar()) != EOF) {

    if (c == '\n') {
      lines++;
    } else if (c == 'e' || c == 'E') {
      continue;
    } else {
      if (is_vowel(c)) {
        vowels++;
      }
    }
    putchar(c);
  }

  printf("I counted this many lines: %d\n", lines);
  printf("I counted this many vowels: %d\n", vowels);
  return 0;
}
